<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tweet;
use App\User;
use Auth;
use DB;
use App\Like;


class TweetController extends Controller
{
    /**
     *Display a Listing of the resource
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){

        //Grab a list of all posts from the model
        $tweets = Tweet:: all();


        return view ('tweets.index', compact ('tweets'));
    }

    public function feed () {

        $id = Auth::id();
        $tweets = Tweet::whereIn('user_id', function($query) use($id)
        {
          $query->select('leader_id')
                ->from('followers')
                ->where('follower_id', $id);
        })->orWhere('user_id', $id)->latest()->get();

        return view('feed', compact('tweets'));

    }

    //Show the form for creating a new resource

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){

        return view('tweets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function store(Request $request){
         $data['title']=request ('title');
         $data['body']=request ('body');
         $data['user_id']=auth()->user()->id;

         $tweet= Tweet::create($data);


         return redirect('/profile/');

     }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response

     */

     public function show($id){
         $tweet= Tweet::find($id);
         return view ('tweets.show', compact(['tweet']));

     }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function edit($id){
         //Load the post
         $tweet = Tweet::find($id);
         if(Auth::id() == $tweet->user->id){

         //pass the post to the edit view
         return view('tweets.edit', compact('tweet'));

        }

     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function update(Request $request, $id){
         $tweet = Tweet::find($id);
         $tweet->title = request('title');
         $tweet->body = request('body');

         $tweet->save();

         return redirect('/profile/');

     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function destroy($id){
         Tweet::destroy($id);

         return redirect('/profile/');
     }

     public function like($id){

         $duplicate=DB::table('likes')
         ->where([
             ['user_id', '=', auth()->id()],
             ['tweet_id', '=', $id]
         ])

         ->count();

         if($duplicate){

             $like = Like::where([
                 ['user_id', '=', auth()->id()],
                 ['tweet_id', '=', $id]

             ])->first();

                 $like->delete();
                return redirect()->back();

         }

         $like = Like::create([
             'user_id'=>auth()->id(),
             'tweet_id'=> $id
         ]);

         return redirect()->back();

     }

}
