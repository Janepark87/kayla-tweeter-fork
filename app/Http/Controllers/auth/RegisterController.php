<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class RegisterController extends Controller
{
    public function create(){
        return view('auth.register.create');

    }

    public function store(Request $request){
        $data = $request->validate([
            'name' =>  'required|min:2|max:30',
            'email'=>   'required|email|max:50',
            'password'=> 'required|min:6|confirmed',
            'username'=> 'required|min:6|max:15'
        ]);

        $data['password'] = bcrypt($data['password']);

        User::create($data);
        session()->flash('message', 'You have successfully registered an account');
        return redirect ('/auth/login');


    }

}
