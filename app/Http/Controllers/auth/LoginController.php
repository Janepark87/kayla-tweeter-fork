<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function __construct(){

        $this->middleware('guest')->except('destroy');


    }

    public function create(){
        return view('auth.login.create');

    }
    public function store(Request $request){

        if(! auth()->attempt(request(['email', 'password']))){
            session()->flash('errorMessage', 'Invalid credentials try again');
            return redirect()->back();

        }
        session()->flash('message', 'You have successfully logged in');
        return redirect('/profile');

    }

    public function destroy(){
        auth()->logout();
        return redirect('/');

    }

}
