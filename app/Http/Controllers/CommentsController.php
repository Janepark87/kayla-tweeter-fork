<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tweet;
use App\Comment;
use Auth;

class CommentsController extends Controller
{
    public function store(Tweet $tweet){

        Comment::create([
            'body'=>request('body'),
            'tweet_id'=> $tweet->id,
            'user_id'=> auth()->id()
        ]);

        return back();
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function edit($id){
        //Load the post
        $comment = Comment::find($id);
        if(Auth::id() == $comment->user->id){
        //pass the post to the edit view

        return view('comments.edit', compact('comment'));
        }
    }


    /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $comment = Comment::find($id);
            $comment->body = request('body');

            $comment->save();

            return redirect('/feed');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            Comment::destroy($id);

            return redirect('/profile');
        }


}
