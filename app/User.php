<?php

namespace App;

use App\Tweet;
use App\Comment;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Profile;
use App\Like;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username','website','birthday','location','bio',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tweets(){
        return $this->hasMany(Tweet::class);
    }

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    /**
 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
 */

    public function followers(){
        return $this->belongsToMany(User::class, 'followers', 'leader_id', 'follower_id')->withTimestamps();
    }

    /**
 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
 */
    public function followings(){
        return $this->belongsToMany(User::class, 'followers', 'follower_id', 'leader_id')->withTimestamps();
    }

    public function likes(){
        return $this->hasMany('App\like');
    }




}
