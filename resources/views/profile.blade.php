

@include('layouts/nav')



    <div class="jumbotron">
        <span class="pull-right">
        <a href="/followers" class="btn btn-outline-info btn-sm">Followers</a>
        <a href="/following" class="btn btn-outline-warning btn-sm">Following</a>
        </span>
           <div class="container">


              <img src="/uploads/avatars/{{ $user->avatar }}" style="width:150px; height=150px; border-radius:50%; margin-right:25px; float:left;">

             <h2 class="display-4">{{ $user->username }}'s Profile</h2>
             <h5><a href="{{ $user->website }}">Website: {{ $user->website }}</a></h5>
             <h5>Birthday: {{ $user->birthday }}</h5>
             <h5>Location: {{ $user->location }}</h5>


             @if(Auth::id() == $user->id)
              <br />
              <br />
              <br />
             <form enctype="multipart/form-data" action="/profile" method="POST">
                 <label>Update Profile Image</label>
                 <br />
                 <input type="file" name="avatar">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="submit" class="pull-left btn btn-xs btn-primary">

             </form>
             @else
             <a class="btn btn-primary btn-sm" href="{{ route('user.follow', $user->id) }}" role="button">Follow </a>
              <a class="btn btn-primary btn-sm btn-danger" href="{{ route('user.unfollow', $user->id) }}" role="button">Unfollow</a>
             @endif
              <br />
               <br />


             <br />
             <p>Bio: {{ $user->bio }}</p>

                 <p><a class="btn btn-primary btn-md" href="/tweets/create" role="button">Write a Tweet</a></p>

           </div>
         </div>

<div class="container">

    <h2>{{ $user->username }}'s Tweets</h2>
    @extends('tweets/index')
    @yield('tweets')

</div>


</html>
