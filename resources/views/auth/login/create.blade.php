@extends('home')

@section('form')
<h1>Login</h1>

<form action="/auth/login" method="POST">
    @csrf
    <div class="form-group">
        <label>Email</label>

        <input type="email" name="email" class="form-control"/>

    </div><!---End of form-group--->


    <div class="form-group">
        <label>Password</label>

        <input type="password" name="password" class="form-control"/>

    </div><!---End of form-group--->

    @if($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <div class="form-group">

        <button type="submit" class="btn btn-primary btn-lg">Login</button>

    </div><!---End of form-group--->

</form>

@endsection
